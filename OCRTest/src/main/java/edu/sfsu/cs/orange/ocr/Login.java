package edu.sfsu.cs.orange.ocr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import edu.sfsu.cs.orange.ocr.R;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends Activity {
    OkHttpClient client;
    public static final MediaType JSON = MediaType.get("application/json");
    String endpoint = "http://Cardtracker-env-1.2ttx4efdnq.us-east-1.elasticbeanstalk.com";
    EditText username;
    EditText password;
    StringBuffer url;
    public static final String ex_mess = "userId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        client = new OkHttpClient();
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
    }

    public void log_in (View view) throws IOException{
        String user = username.getText().toString();
        String pass = password.getText().toString();
        login(user, pass);
    }


    void login(String user, String pass) throws IOException {
        url = new StringBuffer(endpoint);
        url.append("/login");
        Map<String, String> info = new HashMap<String,String>();
        info.put("email", user);
        info.put("password",pass);
        JSONObject pkg = new JSONObject(info);
        Log.i("update","Shit");

        RequestBody body = RequestBody.create(JSON, pkg.toString());
        Request request = new Request.Builder()
                .url(url.toString())
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        Log.i("update","Putting Extra");
        if(response != null){
            try {
                String rtext = response.body().string();
                JSONObject jdata = new JSONObject(rtext);
                Log.i("ff",jdata.toString());
                JSONArray jarray = jdata.getJSONArray("user_info");
                String user_id = jarray.get(0).toString();
                Intent intent = new Intent(this, CaptureActivity.class);
                intent.putExtra(ex_mess, user_id);
                startActivity(intent);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
